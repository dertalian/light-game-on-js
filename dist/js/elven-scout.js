/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/Game.js":
/*!*********************!*\
  !*** ./src/Game.js ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Game": () => (/* binding */ Game)
/* harmony export */ });
/* harmony import */ var _Scene__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Scene */ "./src/Scene.js");
/* harmony import */ var _scenes_Loading__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scenes/Loading */ "./src/scenes/Loading.js");
/* harmony import */ var _scenes_Menu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scenes/Menu */ "./src/scenes/Menu.js");
/* harmony import */ var _Screen__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Screen */ "./src/Screen.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }





var Game = /*#__PURE__*/function () {
  function Game() {
    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$width = _ref.width,
        width = _ref$width === void 0 ? 400 : _ref$width,
        _ref$height = _ref.height,
        height = _ref$height === void 0 ? 400 : _ref$height;

    _classCallCheck(this, Game);

    this.screen = new _Screen__WEBPACK_IMPORTED_MODULE_3__.Screen(width, height);
    this.screen.loadImages({
      orc: 'img/orc.png',
      player: 'img/player.png',
      title: 'img/title.jpg',
      tiles: 'img/tiles.png'
    });
    this.scenes = {
      menu: new _scenes_Menu__WEBPACK_IMPORTED_MODULE_2__.Menu(this),
      loading: new _scenes_Loading__WEBPACK_IMPORTED_MODULE_1__.Loading(this)
    };
    this.currentScene = this.scenes.loading;
    this.currentScene.init();
  }

  _createClass(Game, [{
    key: "changeScene",
    value: function changeScene(status) {
      switch (status) {
        case _Scene__WEBPACK_IMPORTED_MODULE_0__.Scene.LOADED:
          return this.scenes.menu;

        default:
          return this.scenes.menu;
      }
    }
  }, {
    key: "frame",
    value: function frame(time) {
      var _this = this;

      if (this.currentScene.status != _Scene__WEBPACK_IMPORTED_MODULE_0__.Scene.WORKING) {
        this.currentScene = this.changeScene(this.currentScene.status);
        this.currentScene.init();
      }

      this.currentScene.render(time);
      requestAnimationFrame(function (time) {
        return _this.frame(time);
      });
    }
  }, {
    key: "run",
    value: function run() {
      var _this2 = this;

      requestAnimationFrame(function (time) {
        return _this2.frame(time);
      });
    }
  }]);

  return Game;
}();

/***/ }),

/***/ "./src/ImageLoader.js":
/*!****************************!*\
  !*** ./src/ImageLoader.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ImageLoader": () => (/* binding */ ImageLoader)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ImageLoader = /*#__PURE__*/function () {
  function ImageLoader(imageFiles) {
    _classCallCheck(this, ImageLoader);

    this.imageFiles = imageFiles;
    this.images = {};
  }

  _createClass(ImageLoader, [{
    key: "load",
    value: function load() {
      var promises = [];

      for (var name in this.imageFiles) {
        promises.push(this.loadImages(name, this.imageFiles[name]));
      }

      return Promise.all(promises);
    }
  }, {
    key: "loadImages",
    value: function loadImages(name, src) {
      var _this = this;

      return new Promise(function (resolve) {
        var image = new Image();
        _this.images[name] = image;
        image.src = src;

        image.onload = function () {
          return resolve(name);
        };
      });
    }
  }]);

  return ImageLoader;
}();

/***/ }),

/***/ "./src/Scene.js":
/*!**********************!*\
  !*** ./src/Scene.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Scene": () => (/* binding */ Scene)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Scene = /*#__PURE__*/function () {
  function Scene(game) {
    _classCallCheck(this, Scene);

    this.game = game;
    this.status = this.constructor.WORKING;
  }

  _createClass(Scene, [{
    key: "init",
    value: function init() {
      this.status = this.constructor.WORKING;
    }
  }, {
    key: "finish",
    value: function finish(status) {
      this.status = status;
    }
  }, {
    key: "render",
    value: function render(time) {}
  }], [{
    key: "WORKING",
    get: function get() {
      return 'WORKING';
    }
  }, {
    key: "LOADED",
    get: function get() {
      return 'LOADED';
    }
  }, {
    key: "START_GAME",
    get: function get() {
      return 'START_GAME';
    }
  }, {
    key: "GAME_OVER",
    get: function get() {
      return 'GAME_OVER';
    }
  }, {
    key: "GAME_WIN",
    get: function get() {
      return 'GAME_WIN';
    }
  }, {
    key: "FINISHED",
    get: function get() {
      return 'FINISHED';
    }
  }]);

  return Scene;
}();

/***/ }),

/***/ "./src/Screen.js":
/*!***********************!*\
  !*** ./src/Screen.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Screen": () => (/* binding */ Screen)
/* harmony export */ });
/* harmony import */ var _ImageLoader__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ImageLoader */ "./src/ImageLoader.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }


var Screen = /*#__PURE__*/function () {
  function Screen(width, height) {
    _classCallCheck(this, Screen);

    this.height = height;
    this.width = width;
    this.canvas = this.createCanvas(width, height);
    this.context = this.canvas.getContext('2d');
    this.images = {};
    this.isImagesLoaded = false;
  }

  _createClass(Screen, [{
    key: "loadImages",
    value: function loadImages(imageFiles) {
      var _this = this;

      var loader = new _ImageLoader__WEBPACK_IMPORTED_MODULE_0__.ImageLoader(imageFiles);
      loader.load().then(function (names) {
        Object.assign(_this.images, loader.images);
        _this.isImagesLoaded = true;
      });
    }
  }, {
    key: "createCanvas",
    value: function createCanvas(width, height) {
      var canvas = document.querySelector('canvas');

      if (!canvas) {
        canvas = document.createElement('canvas');
        document.body.append(canvas);
      }

      canvas.width = width;
      canvas.height = height;
      return canvas;
    }
  }, {
    key: "fill",
    value: function fill(color) {
      this.context.fillStyle = color;
      this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
    }
  }, {
    key: "print",
    value: function print(text, x, y) {
      this.context.fillStyle = '#FFFFFF';
      this.context.font = "22px Sans-Serif";
      this.context.fillText(text, x, y);
    }
  }, {
    key: "drawImage",
    value: function drawImage(imageName, x, y) {
      this.context.drawImage(this.images[imageName], x, y);
    }
  }]);

  return Screen;
}();

/***/ }),

/***/ "./src/scenes/Loading.js":
/*!*******************************!*\
  !*** ./src/scenes/Loading.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Loading": () => (/* binding */ Loading)
/* harmony export */ });
/* harmony import */ var _Scene__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Scene */ "./src/Scene.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


var Loading = /*#__PURE__*/function (_Scene) {
  _inherits(Loading, _Scene);

  var _super = _createSuper(Loading);

  function Loading(game) {
    var _this;

    _classCallCheck(this, Loading);

    _this = _super.call(this, game);
    _this.loadedAt = 0;
    return _this;
  }

  _createClass(Loading, [{
    key: "init",
    value: function init() {
      _get(_getPrototypeOf(Loading.prototype), "init", this).call(this);

      this.loadedAt = 0;
    }
  }, {
    key: "update",
    value: function update(time) {
      if (this.loadedAt == 0 && this.game.screen.isImagesLoaded == true) {
        this.loadedAt = time;
      }

      if (this.loadedAt != 0 && time - this.loadedAt > 500) {
        this.finish(_Scene__WEBPACK_IMPORTED_MODULE_0__.Scene.LOADED);
      }
    }
  }, {
    key: "render",
    value: function render(time) {
      this.update(time);
      this.game.screen.fill('#000000');
      this.game.screen.print(50, 70, 'Загрузка...');

      _get(_getPrototypeOf(Loading.prototype), "render", this).call(this, time);
    }
  }]);

  return Loading;
}(_Scene__WEBPACK_IMPORTED_MODULE_0__.Scene);

/***/ }),

/***/ "./src/scenes/Menu.js":
/*!****************************!*\
  !*** ./src/scenes/Menu.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Menu": () => (/* binding */ Menu)
/* harmony export */ });
/* harmony import */ var _Scene__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Scene */ "./src/Scene.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }


var Menu = /*#__PURE__*/function (_Scene) {
  _inherits(Menu, _Scene);

  var _super = _createSuper(Menu);

  function Menu(game) {
    _classCallCheck(this, Menu);

    return _super.call(this, game);
  }

  _createClass(Menu, [{
    key: "init",
    value: function init() {
      _get(_getPrototypeOf(Menu.prototype), "init", this).call(this);
    }
  }, {
    key: "render",
    value: function render(time) {
      this.game.screen.drawImage(0, 0, 'title');

      _get(_getPrototypeOf(Menu.prototype), "render", this).call(this, time);
    }
  }]);

  return Menu;
}(_Scene__WEBPACK_IMPORTED_MODULE_0__.Scene);

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Game__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Game */ "./src/Game.js");


window.onload = function () {
  var game = new _Game__WEBPACK_IMPORTED_MODULE_0__.Game({
    width: 400,
    height: 400
  });
  game.run();
};
})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianMvZWx2ZW4tc2NvdXQuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFTyxJQUFNSSxJQUFiO0FBQ0Msa0JBQTRDO0FBQUEsbUZBQUosRUFBSTtBQUFBLDBCQUEvQkMsS0FBK0I7QUFBQSxRQUEvQkEsS0FBK0IsMkJBQXZCLEdBQXVCO0FBQUEsMkJBQWxCQyxNQUFrQjtBQUFBLFFBQWxCQSxNQUFrQiw0QkFBWCxHQUFXOztBQUFBOztBQUMzQyxTQUFLQyxNQUFMLEdBQWMsSUFBSUosMkNBQUosQ0FBV0UsS0FBWCxFQUFrQkMsTUFBbEIsQ0FBZDtBQUNBLFNBQUtDLE1BQUwsQ0FBWUMsVUFBWixDQUF1QjtBQUN0QkMsTUFBQUEsR0FBRyxFQUFFLGFBRGlCO0FBRXRCQyxNQUFBQSxNQUFNLEVBQUUsZ0JBRmM7QUFHdEJDLE1BQUFBLEtBQUssRUFBRSxlQUhlO0FBSXRCQyxNQUFBQSxLQUFLLEVBQUU7QUFKZSxLQUF2QjtBQU1BLFNBQUtDLE1BQUwsR0FBYztBQUNiQyxNQUFBQSxJQUFJLEVBQUUsSUFBSVosOENBQUosQ0FBUyxJQUFULENBRE87QUFFYmEsTUFBQUEsT0FBTyxFQUFFLElBQUlkLG9EQUFKLENBQVksSUFBWjtBQUZJLEtBQWQ7QUFJQSxTQUFLZSxZQUFMLEdBQW9CLEtBQUtILE1BQUwsQ0FBWUUsT0FBaEM7QUFDQSxTQUFLQyxZQUFMLENBQWtCQyxJQUFsQjtBQUNBOztBQWZGO0FBQUE7QUFBQSxXQWlCQyxxQkFBWUMsTUFBWixFQUFvQjtBQUNiLGNBQVFBLE1BQVI7QUFDSSxhQUFLbEIsZ0RBQUw7QUFDSSxpQkFBTyxLQUFLYSxNQUFMLENBQVlDLElBQW5COztBQUNKO0FBQ0ksaUJBQU8sS0FBS0QsTUFBTCxDQUFZQyxJQUFuQjtBQUpSO0FBTUg7QUF4Qkw7QUFBQTtBQUFBLFdBMEJDLGVBQU1NLElBQU4sRUFBWTtBQUFBOztBQUNYLFVBQUcsS0FBS0osWUFBTCxDQUFrQkUsTUFBbEIsSUFBNEJsQixpREFBL0IsRUFBOEM7QUFDN0MsYUFBS2dCLFlBQUwsR0FBb0IsS0FBS00sV0FBTCxDQUFpQixLQUFLTixZQUFMLENBQWtCRSxNQUFuQyxDQUFwQjtBQUNBLGFBQUtGLFlBQUwsQ0FBa0JDLElBQWxCO0FBQ0E7O0FBQ0QsV0FBS0QsWUFBTCxDQUFrQk8sTUFBbEIsQ0FBeUJILElBQXpCO0FBQ0FJLE1BQUFBLHFCQUFxQixDQUFDLFVBQUFKLElBQUk7QUFBQSxlQUFJLEtBQUksQ0FBQ0ssS0FBTCxDQUFXTCxJQUFYLENBQUo7QUFBQSxPQUFMLENBQXJCO0FBQ0E7QUFqQ0Y7QUFBQTtBQUFBLFdBbUNDLGVBQU07QUFBQTs7QUFDTEksTUFBQUEscUJBQXFCLENBQUMsVUFBQUosSUFBSTtBQUFBLGVBQUksTUFBSSxDQUFDSyxLQUFMLENBQVdMLElBQVgsQ0FBSjtBQUFBLE9BQUwsQ0FBckI7QUFDQTtBQXJDRjs7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0xPLElBQU1NLFdBQWI7QUFDQyx1QkFBWUMsVUFBWixFQUF3QjtBQUFBOztBQUN2QixTQUFLQSxVQUFMLEdBQWtCQSxVQUFsQjtBQUNBLFNBQUtDLE1BQUwsR0FBYyxFQUFkO0FBQ0E7O0FBSkY7QUFBQTtBQUFBLFdBS0MsZ0JBQU87QUFDTixVQUFNQyxRQUFRLEdBQUcsRUFBakI7O0FBQ0EsV0FBSSxJQUFJQyxJQUFSLElBQWdCLEtBQUtILFVBQXJCLEVBQWlDO0FBQ2hDRSxRQUFBQSxRQUFRLENBQUNFLElBQVQsQ0FBYyxLQUFLdkIsVUFBTCxDQUFnQnNCLElBQWhCLEVBQXNCLEtBQUtILFVBQUwsQ0FBZ0JHLElBQWhCLENBQXRCLENBQWQ7QUFDQTs7QUFDRCxhQUFPRSxPQUFPLENBQUNDLEdBQVIsQ0FBWUosUUFBWixDQUFQO0FBQ0E7QUFYRjtBQUFBO0FBQUEsV0FhQyxvQkFBV0MsSUFBWCxFQUFpQkksR0FBakIsRUFBc0I7QUFBQTs7QUFDckIsYUFBTyxJQUFJRixPQUFKLENBQVksVUFBQ0csT0FBRCxFQUFhO0FBQy9CLFlBQU1DLEtBQUssR0FBRyxJQUFJQyxLQUFKLEVBQWQ7QUFDQSxhQUFJLENBQUNULE1BQUwsQ0FBWUUsSUFBWixJQUFvQk0sS0FBcEI7QUFDQUEsUUFBQUEsS0FBSyxDQUFDRixHQUFOLEdBQVlBLEdBQVo7O0FBQ0FFLFFBQUFBLEtBQUssQ0FBQ0UsTUFBTixHQUFlO0FBQUEsaUJBQU1ILE9BQU8sQ0FBQ0wsSUFBRCxDQUFiO0FBQUEsU0FBZjtBQUNBLE9BTE0sQ0FBUDtBQU1BO0FBcEJGOztBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQU8sSUFBTTlCLEtBQWI7QUFDQyxpQkFBWXVDLElBQVosRUFBa0I7QUFBQTs7QUFDakIsU0FBS0EsSUFBTCxHQUFZQSxJQUFaO0FBQ0EsU0FBS3JCLE1BQUwsR0FBYyxLQUFLc0IsV0FBTCxDQUFpQm5CLE9BQS9CO0FBQ0E7O0FBSkY7QUFBQTtBQUFBLFdBYUMsZ0JBQU87QUFDTixXQUFLSCxNQUFMLEdBQWMsS0FBS3NCLFdBQUwsQ0FBaUJuQixPQUEvQjtBQUNBO0FBZkY7QUFBQTtBQUFBLFdBaUJDLGdCQUFPSCxNQUFQLEVBQWU7QUFDZCxXQUFLQSxNQUFMLEdBQWNBLE1BQWQ7QUFDQTtBQW5CRjtBQUFBO0FBQUEsV0FxQkMsZ0JBQU9FLElBQVAsRUFBYSxDQUVaO0FBdkJGO0FBQUE7QUFBQSxTQU1DLGVBQXFCO0FBQUUsYUFBTyxTQUFQO0FBQW1CO0FBTjNDO0FBQUE7QUFBQSxTQU9JLGVBQW9CO0FBQUUsYUFBTyxRQUFQO0FBQWtCO0FBUDVDO0FBQUE7QUFBQSxTQVFJLGVBQXdCO0FBQUUsYUFBTyxZQUFQO0FBQXNCO0FBUnBEO0FBQUE7QUFBQSxTQVNJLGVBQXVCO0FBQUUsYUFBTyxXQUFQO0FBQXFCO0FBVGxEO0FBQUE7QUFBQSxTQVVJLGVBQXNCO0FBQUUsYUFBTyxVQUFQO0FBQW9CO0FBVmhEO0FBQUE7QUFBQSxTQVdJLGVBQXNCO0FBQUUsYUFBTyxVQUFQO0FBQW9CO0FBWGhEOztBQUFBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBRU8sSUFBTWpCLE1BQWI7QUFDQyxrQkFBWUUsS0FBWixFQUFtQkMsTUFBbkIsRUFBMkI7QUFBQTs7QUFDMUIsU0FBS0EsTUFBTCxHQUFjQSxNQUFkO0FBQ0EsU0FBS0QsS0FBTCxHQUFhQSxLQUFiO0FBQ0EsU0FBS29DLE1BQUwsR0FBYyxLQUFLQyxZQUFMLENBQWtCckMsS0FBbEIsRUFBeUJDLE1BQXpCLENBQWQ7QUFDQSxTQUFLcUMsT0FBTCxHQUFlLEtBQUtGLE1BQUwsQ0FBWUcsVUFBWixDQUF1QixJQUF2QixDQUFmO0FBQ0EsU0FBS2hCLE1BQUwsR0FBYyxFQUFkO0FBQ0EsU0FBS2lCLGNBQUwsR0FBc0IsS0FBdEI7QUFDQTs7QUFSRjtBQUFBO0FBQUEsV0FVQyxvQkFBV2xCLFVBQVgsRUFBdUI7QUFBQTs7QUFDdEIsVUFBTW1CLE1BQU0sR0FBRyxJQUFJcEIscURBQUosQ0FBZ0JDLFVBQWhCLENBQWY7QUFDQW1CLE1BQUFBLE1BQU0sQ0FBQ0MsSUFBUCxHQUFjQyxJQUFkLENBQW1CLFVBQUNDLEtBQUQsRUFBVztBQUM3QkMsUUFBQUEsTUFBTSxDQUFDQyxNQUFQLENBQWMsS0FBSSxDQUFDdkIsTUFBbkIsRUFBMkJrQixNQUFNLENBQUNsQixNQUFsQztBQUNBLGFBQUksQ0FBQ2lCLGNBQUwsR0FBc0IsSUFBdEI7QUFDQSxPQUhEO0FBSUE7QUFoQkY7QUFBQTtBQUFBLFdBa0JDLHNCQUFheEMsS0FBYixFQUFvQkMsTUFBcEIsRUFBNEI7QUFDM0IsVUFBSW1DLE1BQU0sR0FBR1csUUFBUSxDQUFDQyxhQUFULENBQXVCLFFBQXZCLENBQWI7O0FBQ0EsVUFBRyxDQUFDWixNQUFKLEVBQVk7QUFDWEEsUUFBQUEsTUFBTSxHQUFHVyxRQUFRLENBQUNFLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBVDtBQUNBRixRQUFBQSxRQUFRLENBQUNHLElBQVQsQ0FBY0MsTUFBZCxDQUFxQmYsTUFBckI7QUFDQTs7QUFDREEsTUFBQUEsTUFBTSxDQUFDcEMsS0FBUCxHQUFlQSxLQUFmO0FBQ0FvQyxNQUFBQSxNQUFNLENBQUNuQyxNQUFQLEdBQWdCQSxNQUFoQjtBQUVBLGFBQU9tQyxNQUFQO0FBQ0E7QUE1QkY7QUFBQTtBQUFBLFdBOEJDLGNBQUtnQixLQUFMLEVBQVk7QUFDWCxXQUFLZCxPQUFMLENBQWFlLFNBQWIsR0FBeUJELEtBQXpCO0FBQ0EsV0FBS2QsT0FBTCxDQUFhZ0IsUUFBYixDQUFzQixDQUF0QixFQUF3QixDQUF4QixFQUEyQixLQUFLbEIsTUFBTCxDQUFZcEMsS0FBdkMsRUFBOEMsS0FBS29DLE1BQUwsQ0FBWW5DLE1BQTFEO0FBQ0E7QUFqQ0Y7QUFBQTtBQUFBLFdBbUNDLGVBQU1zRCxJQUFOLEVBQVlDLENBQVosRUFBZUMsQ0FBZixFQUFrQjtBQUNqQixXQUFLbkIsT0FBTCxDQUFhZSxTQUFiLEdBQXlCLFNBQXpCO0FBQ0EsV0FBS2YsT0FBTCxDQUFhb0IsSUFBYixHQUFvQixpQkFBcEI7QUFDQSxXQUFLcEIsT0FBTCxDQUFhcUIsUUFBYixDQUFzQkosSUFBdEIsRUFBNEJDLENBQTVCLEVBQStCQyxDQUEvQjtBQUNBO0FBdkNGO0FBQUE7QUFBQSxXQXlDQyxtQkFBVUcsU0FBVixFQUFxQkosQ0FBckIsRUFBd0JDLENBQXhCLEVBQTJCO0FBQzFCLFdBQUtuQixPQUFMLENBQWF1QixTQUFiLENBQXVCLEtBQUt0QyxNQUFMLENBQVlxQyxTQUFaLENBQXZCLEVBQStDSixDQUEvQyxFQUFrREMsQ0FBbEQ7QUFDQTtBQTNDRjs7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBRU8sSUFBTTdELE9BQWI7QUFBQTs7QUFBQTs7QUFDQyxtQkFBWXNDLElBQVosRUFBa0I7QUFBQTs7QUFBQTs7QUFDakIsOEJBQU1BLElBQU47QUFDQSxVQUFLNEIsUUFBTCxHQUFnQixDQUFoQjtBQUZpQjtBQUdqQjs7QUFKRjtBQUFBO0FBQUEsV0FNQyxnQkFBTztBQUNOOztBQUNBLFdBQUtBLFFBQUwsR0FBZ0IsQ0FBaEI7QUFDQTtBQVRGO0FBQUE7QUFBQSxXQVdDLGdCQUFPL0MsSUFBUCxFQUFhO0FBQ04sVUFBRyxLQUFLK0MsUUFBTCxJQUFpQixDQUFqQixJQUFzQixLQUFLNUIsSUFBTCxDQUFVaEMsTUFBVixDQUFpQnNDLGNBQWpCLElBQW1DLElBQTVELEVBQWtFO0FBQzlELGFBQUtzQixRQUFMLEdBQWdCL0MsSUFBaEI7QUFDSDs7QUFDRCxVQUFHLEtBQUsrQyxRQUFMLElBQWlCLENBQWpCLElBQXVCL0MsSUFBSSxHQUFHLEtBQUsrQyxRQUFiLEdBQXlCLEdBQWxELEVBQXVEO0FBQ25ELGFBQUtDLE1BQUwsQ0FBWXBFLGdEQUFaO0FBQ0g7QUFDSjtBQWxCTDtBQUFBO0FBQUEsV0FvQkMsZ0JBQU9vQixJQUFQLEVBQWE7QUFDWixXQUFLaUQsTUFBTCxDQUFZakQsSUFBWjtBQUNBLFdBQUttQixJQUFMLENBQVVoQyxNQUFWLENBQWlCK0QsSUFBakIsQ0FBc0IsU0FBdEI7QUFDQSxXQUFLL0IsSUFBTCxDQUFVaEMsTUFBVixDQUFpQmdFLEtBQWpCLENBQXVCLEVBQXZCLEVBQTBCLEVBQTFCLEVBQTZCLGFBQTdCOztBQUNBLDBFQUFhbkQsSUFBYjtBQUNBO0FBekJGOztBQUFBO0FBQUEsRUFBNkJwQix5Q0FBN0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRkE7QUFFTyxJQUFNRSxJQUFiO0FBQUE7O0FBQUE7O0FBQ0MsZ0JBQVlxQyxJQUFaLEVBQWtCO0FBQUE7O0FBQUEsNkJBQ1hBLElBRFc7QUFFakI7O0FBSEY7QUFBQTtBQUFBLFdBS0MsZ0JBQU87QUFDTjtBQUNBO0FBUEY7QUFBQTtBQUFBLFdBU0MsZ0JBQU9uQixJQUFQLEVBQWE7QUFDWixXQUFLbUIsSUFBTCxDQUFVaEMsTUFBVixDQUFpQjJELFNBQWpCLENBQTJCLENBQTNCLEVBQThCLENBQTlCLEVBQWlDLE9BQWpDOztBQUNBLHVFQUFhOUMsSUFBYjtBQUNBO0FBWkY7O0FBQUE7QUFBQSxFQUEwQnBCLHlDQUExQjs7Ozs7O1VDRkE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTs7VUFFQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTs7Ozs7V0N0QkE7V0FDQTtXQUNBO1dBQ0E7V0FDQSx5Q0FBeUMsd0NBQXdDO1dBQ2pGO1dBQ0E7V0FDQTs7Ozs7V0NQQTs7Ozs7V0NBQTtXQUNBO1dBQ0E7V0FDQSx1REFBdUQsaUJBQWlCO1dBQ3hFO1dBQ0EsZ0RBQWdELGFBQWE7V0FDN0Q7Ozs7Ozs7Ozs7OztBQ05BOztBQUVBd0UsTUFBTSxDQUFDbEMsTUFBUCxHQUFnQixZQUFNO0FBQ3JCLE1BQU1DLElBQUksR0FBRyxJQUFJbkMsdUNBQUosQ0FBUztBQUNyQkMsSUFBQUEsS0FBSyxFQUFFLEdBRGM7QUFFckJDLElBQUFBLE1BQU0sRUFBRTtBQUZhLEdBQVQsQ0FBYjtBQUlBaUMsRUFBQUEsSUFBSSxDQUFDa0MsR0FBTDtBQUNBLENBTkQsQyIsInNvdXJjZXMiOlsid2VicGFjazovL2xpZ2h0LWdhbWUvLi9zcmMvR2FtZS5qcyIsIndlYnBhY2s6Ly9saWdodC1nYW1lLy4vc3JjL0ltYWdlTG9hZGVyLmpzIiwid2VicGFjazovL2xpZ2h0LWdhbWUvLi9zcmMvU2NlbmUuanMiLCJ3ZWJwYWNrOi8vbGlnaHQtZ2FtZS8uL3NyYy9TY3JlZW4uanMiLCJ3ZWJwYWNrOi8vbGlnaHQtZ2FtZS8uL3NyYy9zY2VuZXMvTG9hZGluZy5qcyIsIndlYnBhY2s6Ly9saWdodC1nYW1lLy4vc3JjL3NjZW5lcy9NZW51LmpzIiwid2VicGFjazovL2xpZ2h0LWdhbWUvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vbGlnaHQtZ2FtZS93ZWJwYWNrL3J1bnRpbWUvZGVmaW5lIHByb3BlcnR5IGdldHRlcnMiLCJ3ZWJwYWNrOi8vbGlnaHQtZ2FtZS93ZWJwYWNrL3J1bnRpbWUvaGFzT3duUHJvcGVydHkgc2hvcnRoYW5kIiwid2VicGFjazovL2xpZ2h0LWdhbWUvd2VicGFjay9ydW50aW1lL21ha2UgbmFtZXNwYWNlIG9iamVjdCIsIndlYnBhY2s6Ly9saWdodC1nYW1lLy4vc3JjL2luZGV4LmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNjZW5lIH0gZnJvbSAnLi9TY2VuZSdcbmltcG9ydCB7IExvYWRpbmcgfSBmcm9tICcuL3NjZW5lcy9Mb2FkaW5nJ1xuaW1wb3J0IHsgTWVudSB9IGZyb20gJy4vc2NlbmVzL01lbnUnXG5pbXBvcnQgeyBTY3JlZW4gfSBmcm9tICcuL1NjcmVlbidcblxuZXhwb3J0IGNsYXNzIEdhbWUge1xuXHRjb25zdHJ1Y3Rvcih7d2lkdGggPSA0MDAsIGhlaWdodD00MDB9ID0ge30pIHtcblx0XHR0aGlzLnNjcmVlbiA9IG5ldyBTY3JlZW4od2lkdGgsIGhlaWdodClcblx0XHR0aGlzLnNjcmVlbi5sb2FkSW1hZ2VzKHtcblx0XHRcdG9yYzogJ2ltZy9vcmMucG5nJyxcblx0XHRcdHBsYXllcjogJ2ltZy9wbGF5ZXIucG5nJyxcblx0XHRcdHRpdGxlOiAnaW1nL3RpdGxlLmpwZycsXG5cdFx0XHR0aWxlczogJ2ltZy90aWxlcy5wbmcnICAgICAgICAgICAgXG4gICAgICAgIH0pXG5cdFx0dGhpcy5zY2VuZXMgPSB7XG5cdFx0XHRtZW51OiBuZXcgTWVudSh0aGlzKSxcblx0XHRcdGxvYWRpbmc6IG5ldyBMb2FkaW5nKHRoaXMpXG5cdFx0fVxuXHRcdHRoaXMuY3VycmVudFNjZW5lID0gdGhpcy5zY2VuZXMubG9hZGluZ1xuXHRcdHRoaXMuY3VycmVudFNjZW5lLmluaXQoKVxuXHR9XG5cblx0Y2hhbmdlU2NlbmUoc3RhdHVzKSB7XG4gICAgICAgIHN3aXRjaCAoc3RhdHVzKSB7XG4gICAgICAgICAgICBjYXNlIFNjZW5lLkxPQURFRDpcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zY2VuZXMubWVudVxuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zY2VuZXMubWVudVxuICAgICAgICB9XG4gICAgfVxuXG5cdGZyYW1lKHRpbWUpIHtcdFxuXHRcdGlmKHRoaXMuY3VycmVudFNjZW5lLnN0YXR1cyAhPSBTY2VuZS5XT1JLSU5HKSB7XG5cdFx0XHR0aGlzLmN1cnJlbnRTY2VuZSA9IHRoaXMuY2hhbmdlU2NlbmUodGhpcy5jdXJyZW50U2NlbmUuc3RhdHVzKVxuXHRcdFx0dGhpcy5jdXJyZW50U2NlbmUuaW5pdCgpXG5cdFx0fVxuXHRcdHRoaXMuY3VycmVudFNjZW5lLnJlbmRlcih0aW1lKVxuXHRcdHJlcXVlc3RBbmltYXRpb25GcmFtZSh0aW1lID0+IHRoaXMuZnJhbWUodGltZSkpXG5cdH1cblxuXHRydW4oKSB7XG5cdFx0cmVxdWVzdEFuaW1hdGlvbkZyYW1lKHRpbWUgPT4gdGhpcy5mcmFtZSh0aW1lKSlcblx0fVxufSIsImV4cG9ydCBjbGFzcyBJbWFnZUxvYWRlciB7XG5cdGNvbnN0cnVjdG9yKGltYWdlRmlsZXMpIHtcblx0XHR0aGlzLmltYWdlRmlsZXMgPSBpbWFnZUZpbGVzXG5cdFx0dGhpcy5pbWFnZXMgPSB7fVxuXHR9XG5cdGxvYWQoKSB7XG5cdFx0Y29uc3QgcHJvbWlzZXMgPSBbXVxuXHRcdGZvcihsZXQgbmFtZSBpbiB0aGlzLmltYWdlRmlsZXMpIHtcblx0XHRcdHByb21pc2VzLnB1c2godGhpcy5sb2FkSW1hZ2VzKG5hbWUsIHRoaXMuaW1hZ2VGaWxlc1tuYW1lXSkpXG5cdFx0fVxuXHRcdHJldHVybiBQcm9taXNlLmFsbChwcm9taXNlcylcblx0fVxuXG5cdGxvYWRJbWFnZXMobmFtZSwgc3JjKSB7XG5cdFx0cmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XG5cdFx0XHRjb25zdCBpbWFnZSA9IG5ldyBJbWFnZSgpXG5cdFx0XHR0aGlzLmltYWdlc1tuYW1lXSA9IGltYWdlXG5cdFx0XHRpbWFnZS5zcmMgPSBzcmNcblx0XHRcdGltYWdlLm9ubG9hZCA9ICgpID0+IHJlc29sdmUobmFtZSlcblx0XHR9KVxuXHR9XG59IiwiZXhwb3J0IGNsYXNzIFNjZW5lIHtcblx0Y29uc3RydWN0b3IoZ2FtZSkge1xuXHRcdHRoaXMuZ2FtZSA9IGdhbWVcblx0XHR0aGlzLnN0YXR1cyA9IHRoaXMuY29uc3RydWN0b3IuV09SS0lOR1xuXHR9XG5cblx0c3RhdGljIGdldCBXT1JLSU5HKCkgeyByZXR1cm4gJ1dPUktJTkcnOyB9XG4gICAgc3RhdGljIGdldCBMT0FERUQoKSB7IHJldHVybiAnTE9BREVEJzsgfVxuICAgIHN0YXRpYyBnZXQgU1RBUlRfR0FNRSgpIHsgcmV0dXJuICdTVEFSVF9HQU1FJzsgfVxuICAgIHN0YXRpYyBnZXQgR0FNRV9PVkVSKCkgeyByZXR1cm4gJ0dBTUVfT1ZFUic7IH1cbiAgICBzdGF0aWMgZ2V0IEdBTUVfV0lOKCkgeyByZXR1cm4gJ0dBTUVfV0lOJzsgfVxuICAgIHN0YXRpYyBnZXQgRklOSVNIRUQoKSB7IHJldHVybiAnRklOSVNIRUQnOyB9XG5cblx0aW5pdCgpIHtcblx0XHR0aGlzLnN0YXR1cyA9IHRoaXMuY29uc3RydWN0b3IuV09SS0lOR1xuXHR9XG5cblx0ZmluaXNoKHN0YXR1cykge1xuXHRcdHRoaXMuc3RhdHVzID0gc3RhdHVzXG5cdH1cblxuXHRyZW5kZXIodGltZSkge1xuXHRcdFxuXHR9XG59IiwiaW1wb3J0IHsgSW1hZ2VMb2FkZXIgfSBmcm9tICcuL0ltYWdlTG9hZGVyJ1xuXG5leHBvcnQgY2xhc3MgU2NyZWVuIHtcblx0Y29uc3RydWN0b3Iod2lkdGgsIGhlaWdodCkge1xuXHRcdHRoaXMuaGVpZ2h0ID0gaGVpZ2h0XG5cdFx0dGhpcy53aWR0aCA9IHdpZHRoXG5cdFx0dGhpcy5jYW52YXMgPSB0aGlzLmNyZWF0ZUNhbnZhcyh3aWR0aCwgaGVpZ2h0KVxuXHRcdHRoaXMuY29udGV4dCA9IHRoaXMuY2FudmFzLmdldENvbnRleHQoJzJkJylcblx0XHR0aGlzLmltYWdlcyA9IHt9XG5cdFx0dGhpcy5pc0ltYWdlc0xvYWRlZCA9IGZhbHNlXG5cdH1cblxuXHRsb2FkSW1hZ2VzKGltYWdlRmlsZXMpIHtcblx0XHRjb25zdCBsb2FkZXIgPSBuZXcgSW1hZ2VMb2FkZXIoaW1hZ2VGaWxlcylcblx0XHRsb2FkZXIubG9hZCgpLnRoZW4oKG5hbWVzKSA9PiB7XG5cdFx0XHRPYmplY3QuYXNzaWduKHRoaXMuaW1hZ2VzLCBsb2FkZXIuaW1hZ2VzKVxuXHRcdFx0dGhpcy5pc0ltYWdlc0xvYWRlZCA9IHRydWVcblx0XHR9KVxuXHR9XG5cblx0Y3JlYXRlQ2FudmFzKHdpZHRoLCBoZWlnaHQpIHtcblx0XHRsZXQgY2FudmFzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignY2FudmFzJylcblx0XHRpZighY2FudmFzKSB7XG5cdFx0XHRjYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKVxuXHRcdFx0ZG9jdW1lbnQuYm9keS5hcHBlbmQoY2FudmFzKVxuXHRcdH1cblx0XHRjYW52YXMud2lkdGggPSB3aWR0aFxuXHRcdGNhbnZhcy5oZWlnaHQgPSBoZWlnaHQgXG5cblx0XHRyZXR1cm4gY2FudmFzXG5cdH1cblxuXHRmaWxsKGNvbG9yKSB7XG5cdFx0dGhpcy5jb250ZXh0LmZpbGxTdHlsZSA9IGNvbG9yXG5cdFx0dGhpcy5jb250ZXh0LmZpbGxSZWN0KDAsMCwgdGhpcy5jYW52YXMud2lkdGgsIHRoaXMuY2FudmFzLmhlaWdodClcblx0fVxuXG5cdHByaW50KHRleHQsIHgsIHkpIHtcblx0XHR0aGlzLmNvbnRleHQuZmlsbFN0eWxlID0gJyNGRkZGRkYnXG5cdFx0dGhpcy5jb250ZXh0LmZvbnQgPSBcIjIycHggU2Fucy1TZXJpZlwiXG5cdFx0dGhpcy5jb250ZXh0LmZpbGxUZXh0KHRleHQsIHgsIHkpXG5cdH1cblxuXHRkcmF3SW1hZ2UoaW1hZ2VOYW1lLCB4LCB5KSB7XG5cdFx0dGhpcy5jb250ZXh0LmRyYXdJbWFnZSh0aGlzLmltYWdlc1tpbWFnZU5hbWVdLCB4LCB5KVxuXHR9XG59IiwiaW1wb3J0IHsgU2NlbmUgfSBmcm9tICcuLi9TY2VuZSdcblxuZXhwb3J0IGNsYXNzIExvYWRpbmcgZXh0ZW5kcyBTY2VuZSB7XG5cdGNvbnN0cnVjdG9yKGdhbWUpIHtcblx0XHRzdXBlcihnYW1lKVxuXHRcdHRoaXMubG9hZGVkQXQgPSAwXG5cdH1cblxuXHRpbml0KCkge1xuXHRcdHN1cGVyLmluaXQoKVxuXHRcdHRoaXMubG9hZGVkQXQgPSAwXG5cdH1cblxuXHR1cGRhdGUodGltZSkge1xuICAgICAgICBpZih0aGlzLmxvYWRlZEF0ID09IDAgJiYgdGhpcy5nYW1lLnNjcmVlbi5pc0ltYWdlc0xvYWRlZCA9PSB0cnVlKSB7XG4gICAgICAgICAgICB0aGlzLmxvYWRlZEF0ID0gdGltZVxuICAgICAgICB9XG4gICAgICAgIGlmKHRoaXMubG9hZGVkQXQgIT0gMCAmJiAodGltZSAtIHRoaXMubG9hZGVkQXQpID4gNTAwKSB7XG4gICAgICAgICAgICB0aGlzLmZpbmlzaChTY2VuZS5MT0FERUQpXG4gICAgICAgIH1cbiAgICB9XG5cblx0cmVuZGVyKHRpbWUpIHtcblx0XHR0aGlzLnVwZGF0ZSh0aW1lKVxuXHRcdHRoaXMuZ2FtZS5zY3JlZW4uZmlsbCgnIzAwMDAwMCcpXG5cdFx0dGhpcy5nYW1lLnNjcmVlbi5wcmludCg1MCw3MCwn0JfQsNCz0YDRg9C30LrQsC4uLicpXG5cdFx0c3VwZXIucmVuZGVyKHRpbWUpXG5cdH1cbn0iLCJpbXBvcnQgeyBTY2VuZSB9IGZyb20gJy4uL1NjZW5lJ1xuXG5leHBvcnQgY2xhc3MgTWVudSBleHRlbmRzIFNjZW5lIHtcblx0Y29uc3RydWN0b3IoZ2FtZSkge1xuXHRcdHN1cGVyKGdhbWUpXG5cdH1cblxuXHRpbml0KCkge1xuXHRcdHN1cGVyLmluaXQoKVxuXHR9XG5cblx0cmVuZGVyKHRpbWUpIHtcblx0XHR0aGlzLmdhbWUuc2NyZWVuLmRyYXdJbWFnZSgwLCAwLCAndGl0bGUnKVxuXHRcdHN1cGVyLnJlbmRlcih0aW1lKVxuXHR9XG59IiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIi8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb25zIGZvciBoYXJtb255IGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uZCA9IChleHBvcnRzLCBkZWZpbml0aW9uKSA9PiB7XG5cdGZvcih2YXIga2V5IGluIGRlZmluaXRpb24pIHtcblx0XHRpZihfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZGVmaW5pdGlvbiwga2V5KSAmJiAhX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIGtleSkpIHtcblx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBrZXksIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBkZWZpbml0aW9uW2tleV0gfSk7XG5cdFx0fVxuXHR9XG59OyIsIl9fd2VicGFja19yZXF1aXJlX18ubyA9IChvYmosIHByb3ApID0+IChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBwcm9wKSkiLCIvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSAoZXhwb3J0cykgPT4ge1xuXHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcblx0fVxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xufTsiLCJpbXBvcnQgeyBHYW1lIH0gZnJvbSAnLi9HYW1lJztcblxud2luZG93Lm9ubG9hZCA9ICgpID0+IHtcblx0Y29uc3QgZ2FtZSA9IG5ldyBHYW1lKHtcblx0XHR3aWR0aDogNDAwLFxuXHRcdGhlaWdodDogNDAwXG5cdH0pXG5cdGdhbWUucnVuKClcbn0iXSwibmFtZXMiOlsiU2NlbmUiLCJMb2FkaW5nIiwiTWVudSIsIlNjcmVlbiIsIkdhbWUiLCJ3aWR0aCIsImhlaWdodCIsInNjcmVlbiIsImxvYWRJbWFnZXMiLCJvcmMiLCJwbGF5ZXIiLCJ0aXRsZSIsInRpbGVzIiwic2NlbmVzIiwibWVudSIsImxvYWRpbmciLCJjdXJyZW50U2NlbmUiLCJpbml0Iiwic3RhdHVzIiwiTE9BREVEIiwidGltZSIsIldPUktJTkciLCJjaGFuZ2VTY2VuZSIsInJlbmRlciIsInJlcXVlc3RBbmltYXRpb25GcmFtZSIsImZyYW1lIiwiSW1hZ2VMb2FkZXIiLCJpbWFnZUZpbGVzIiwiaW1hZ2VzIiwicHJvbWlzZXMiLCJuYW1lIiwicHVzaCIsIlByb21pc2UiLCJhbGwiLCJzcmMiLCJyZXNvbHZlIiwiaW1hZ2UiLCJJbWFnZSIsIm9ubG9hZCIsImdhbWUiLCJjb25zdHJ1Y3RvciIsImNhbnZhcyIsImNyZWF0ZUNhbnZhcyIsImNvbnRleHQiLCJnZXRDb250ZXh0IiwiaXNJbWFnZXNMb2FkZWQiLCJsb2FkZXIiLCJsb2FkIiwidGhlbiIsIm5hbWVzIiwiT2JqZWN0IiwiYXNzaWduIiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yIiwiY3JlYXRlRWxlbWVudCIsImJvZHkiLCJhcHBlbmQiLCJjb2xvciIsImZpbGxTdHlsZSIsImZpbGxSZWN0IiwidGV4dCIsIngiLCJ5IiwiZm9udCIsImZpbGxUZXh0IiwiaW1hZ2VOYW1lIiwiZHJhd0ltYWdlIiwibG9hZGVkQXQiLCJmaW5pc2giLCJ1cGRhdGUiLCJmaWxsIiwicHJpbnQiLCJ3aW5kb3ciLCJydW4iXSwic291cmNlUm9vdCI6IiJ9