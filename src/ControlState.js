export class ControlState {
	constructor() {
		this.up = false
		this.down = false
		this.right = false
		this.left = false
		this.fire = false
		this.keyMap = new Map([
			[37,'left'],
			[39,'right'],
			[38,'up'],
			[40,'down'],
			[32,'fire'],
		])
		document.addEventListener('keydown', evt => this.update(evt, true))
		document.addEventListener('keyup', evt => this.update(evt, false))
	}

	update(evt, pressed) {
		if(this.keyMap.get(evt.keyCode)) {
			evt.preventDefault()
			evt.stopPropagation()
			this[this.keyMap.get(evt.keyCode)] = pressed
		}
	}
}