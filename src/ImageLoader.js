export class ImageLoader {
	constructor(imageFiles) {
		this.imageFiles = imageFiles
		this.images = {}
	}
	
	load() {
		const promises = []
		for(let name in this.imageFiles) {
			promises.push(this.loadImages(name, this.imageFiles[name]))
		}
		return Promise.all(promises)
	}

	loadImages(name, src) {
		return new Promise((resolve) => {
			const image = new Image()
			this.images[name] = image
			image.src = src
			image.onload = () => resolve(name)
		})
	}
}