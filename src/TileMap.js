import { Sprite } from './Sprite'

export class TileMap extends Sprite {
	constructor(props) {
		super(props)
		this.hitboxes = props.hitboxes || []
	}
}