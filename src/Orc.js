import { Body } from './Body'
import { Dummy } from './ais/Dummy'

export class Orc extends Body {
	constructor() {
		super({imageName: 'orc', speed: 100})
		this.ai = new Dummy()
		this.ai.control(this)
	}

	update(time) {
		this.ai.update(time)
		super.update(time)
	}
}