import { Scene } from '../Scene'
import { SpriteSheet } from '../SpriteSheet'
import { Player } from '../Player'
import { Orc } from '../Orc'
import { Camera } from '../Camera'
import { Collider } from '../Collider'
import { Arrow } from '../projecttiles/Arrow'

export class GameLevel extends Scene {
	constructor(game) {
		super(game)
		this.tiles = new SpriteSheet({
            imageName: 'tiles',
            imageWidth: 640,
            imageHeight: 640
        })

		this.orc = new Orc()
		this.orc.x = 500
		this.orc.y = 500

		this.arrow = new Arrow('down', 500)

		this.player = new Player(this.game.control)
		this.player.x = 100
		this.player.y = 100

		this.player.addArrow(this.arrow)
		this.collider = new Collider()
	}
	
	init() {
		super.init()
		const mapData = require('../maps/level1.json')
		this.map = this.game.screen.createMap('level1', mapData, this.tiles)
		
		this.mainCamera = new Camera({
            width: this.game.screen.width,
            height: this.game.screen.height,
            limitX: this.map.width - this.game.screen.width,
            limitY: this.map.height - this.game.screen.height
        })
		this.mainCamera.watch(this.player)
		
        this.game.screen.setCamera(this.mainCamera)

		this.collider.addStaticShapes(mapData)
        this.collider.addKinematicBody(this.player)
		this.collider.addKinematicBody(this.orc)
	}

	update(time) {
        this.orc.update(time)
		this.player.update(time)
		this.collider.update(time)
		this.mainCamera.update(time)
		this.arrow.update(time)
    }

	render(time) {
		this.update(time)
		this.game.screen.drawSprite(this.map)
		this.game.screen.drawSprite(this.player.view)
		this.game.screen.drawSprite(this.orc.view)
		if(this.arrow.active) {
            this.game.screen.drawSprite(this.arrow.view);
        }
		super.render()
	}
}