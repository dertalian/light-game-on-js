import { Scene } from '../Scene'

export class Menu extends Scene {
	constructor(game) {
		super(game)
	}

	init() {
		super.init()
	}

	update() {
		if(this.game.control.fire) {
			this.finish(Scene.START_GAME)
		}
	}

	render(time) {
		this.update()
		this.game.screen.drawImage('title', 0, 0)
		this.game.screen.print('Нажмите пробел', 250, 500);
		super.render(time)
	}
}